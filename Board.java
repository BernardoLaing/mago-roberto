import java.awt.Graphics;
import java.util.LinkedList;

import javax.swing.ImageIcon;

public class Board {
	

	private Player player1;
	private Player player2;
	private Player currentPlayer;
	private Word word;
	private ImageIcon bg;
	private LinkedList<Letter> letters;
	private boolean winner;
	private static Board instance;
	
	private Board(){
		word = new Word();
		letters = new LinkedList<Letter>();
		winner = false;
		player1 = new Player(100, 100);
		player2 = new Player(560, 100);
		currentPlayer = player1;
		bg = ImageLoader.getInstance().getImage("bg");
	}
	
	public static Board getInstance() {
		if(instance == null) {
			instance = new Board();
		}
		return instance;
	}
	
	public boolean update() {
		if(word.isGuessed()) {
			return true;
		}
		return false;
	}
	
	public void render(Graphics g) {
		g.drawImage(bg.getImage(), 0, 0, 720, 405, null);
		word.render(g);
		currentPlayer.render(g);
		
	}
	
	public void changeTurn() {
		if(currentPlayer == player1) {
			currentPlayer = player2;
		}else if(currentPlayer == player2) {
			currentPlayer = player1;
		}
	}
	
	public void attempt(char c) {
		boolean state = false;
		for(int i = 0; i < word.getWord().size(); i++) {
			if(word.getWord().get(i).getC() == c) {
				word.getWord().get(i).setGuessed();
				state = true;
			}
		}
		if(!state) {
			currentPlayer.wrong();
		}
	}
	
	
	
	
}

