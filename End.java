import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.ImageIcon;

public class End implements GameState{
	private GameContext gc;
	private ImageIcon num, bg;
	
	public End() {
		bg = ImageLoader.getInstance().getImage("bg");
	}

	@Override
	public void inicio(GameContext gc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void load(GameContext gc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void turn1(GameContext gc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void turn2(GameContext gc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void end(GameContext gc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(Graphics g) {
		g.drawImage(bg.getImage(), 0, 0, 720, 405, null);
		g.setColor(Color.BLACK);
		g.setFont(new Font("arial", 0, 45));
		g.drawString("Game Ended", 200, 200);
		
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setContext(GameContext gc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void restart() {
		// TODO Auto-generated method stub
		
	}
	

}
