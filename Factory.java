
public class Factory {
	
	private static Factory instance;
	
	private Factory() {}
	
	public static Factory getInstance() {
		if(instance == null) {
			instance = new Factory();
		}
		return instance;
	}
	
	public GameState createState(String tag) {
		if(tag.equalsIgnoreCase("inicio"))
			return new Inicio();
		if(tag.equalsIgnoreCase("load"))
			return new Load();
		if(tag.equalsIgnoreCase("t1"))
			return new Turn1();
		if(tag.equalsIgnoreCase("t2"))
			return new Turn2();
		if(tag.equalsIgnoreCase("end"))
			return new End();
		return null;
	}
	
}
