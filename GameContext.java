import java.awt.Graphics;

public class GameContext {
	private GameState inicio, load, turn1, turn2, end, current;
	
	public GameContext() {
		inicio = Factory.getInstance().createState("inicio");
		load =  Factory.getInstance().createState("load");
		turn1 = Factory.getInstance().createState("t1");
		turn2 = Factory.getInstance().createState("t2");
		end = Factory.getInstance().createState("end");
		
		inicio.setContext(this);
		load.setContext(this);
		turn1.setContext(this);
		turn2.setContext(this);
		end.setContext(this);
		
		current = inicio;
	}
	
	public GameState getInicio() {return inicio;}
	public GameState getLoad() {return load;}
	public GameState getTurn1() {return turn1;}
	public GameState getTurn2() {return turn2;}
	public GameState getEnd() {return end;}
	public GameState getCurrent() {return current;}
	
	public void inicio() {current.inicio(this);}
	public void load() {current.load(this);}
	public void turn1() {current.turn1(this);}
	public void turn2() {current.turn2(this);}
	public void end() {current.end(this);}
	
	public void setCurrent(GameState gs) {current = gs;}
	
	public void render(Graphics g) {
		current.render(g);
	}
	
	public void update() {
		current.update();
	}
	

}
