import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class GamePanel extends JPanel implements Runnable{
	private static final long serialVersionUID = 1L;
	private Thread animator;
	private Player p1;
	private Player p2;
	private GameContext context;
	private boolean running;
    private int WIDTH = 720;
    private int HEIGHT =  405;

    public GamePanel() {
    	p1 = new Player(10, 100);
		p2 = new Player(560, 100);
    	setBackground(Color.black);
    	setPreferredSize(new Dimension(WIDTH, HEIGHT));
    	setFocusable(true);
    	requestFocus();
    	context = new GameContext();
    	addMouseListener(new MouseAdapter() {
    		public void mousePressed(MouseEvent e) {
    			if(context.getCurrent() == context.getInicio() && 
    					e.getX() >= 210 && e.getX() <= 510 && 
    					e.getY() >= 240 && e.getY() <= 340) {
    				context.load();
    			}
    		}
    	});
    	addKeyListener(new KeyAdapter() {
    		public void keyPressed(KeyEvent e) {
    			int keyCode = e.getKeyCode();
    			if(context.getCurrent() == context.getTurn1()) {
    				if(keyCode >= 65 && keyCode <= 90) {
        				if(context.getCurrent() == context.getTurn1()) {
            				Board.getInstance().attempt((char)keyCode);
            				System.out.println("Turn 1 attempt: " + (char)keyCode);
            			}
        			}
    			}
    		}
    	});
    	
    }
    
    public void addNotify()	{
        super.addNotify();
        startGame();
    }//addNotify

    private void startGame(){
        if(animator == null || !running){
            animator = new Thread(this);
            animator.start();
        }
    }//startGame()

    public void stopGame(){
        running = false;
    }//stopGame()

    public void run(){
        running = true;
        while(running) {
            gameUpdate();
            gameRender();
            paintScreen();
            readyForTermination();
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
            }
        }

    }//run()
    
    private void gameUpdate(){

        context.update();

    }//gameUpdate()

    private Graphics dbg;
    private Image dbImage = null;

    private void gameRender(){
        if(dbImage == null)
        {
            dbImage = createImage(WIDTH,HEIGHT);
            if(dbImage == null){
                System.out.println("dbImage is null");
                return;
            }else{
                dbg = dbImage.getGraphics();
            }

        }
        dbg.setColor(Color.white);
        dbg.fillRect(0,0,WIDTH,HEIGHT);
        context.render(dbg);
    }//gameRender()


    public void paintComponent(Graphics g){
        super.paintComponent(g);
        if(dbImage != null)
            g.drawImage(dbImage, 0, 0, null);
    }

    private void readyForTermination() {
//        addKeyListener( new KeyAdapter() {
//            public void keyPressed(KeyEvent e) {
//            	int keyCode = e.getKeyCode();
//    			if(keyCode >= 65 && keyCode <= 90) {
//    				if(context.getCurrent() == context.getTurn1()) {
//        				Board.getInstance().attempt((char)keyCode);
//        				System.out.println("Turn 1 attempt: " + (char)keyCode);
//        			}
//    			}
//                try {
//                    if (e.getKeyCode() == KeyEvent.VK_Q) {
//                        System.exit(0);
//                    }
//                    if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
//
//                    } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
//
//                    }
//                    if (e.getKeyCode() == KeyEvent.VK_SPACE) {
//
//                    }
//                } catch(NullPointerException x){
//                    System.out.println("Game has ended");
//                }
//            }
//        });
    }
    
    private void paintScreen(){
        Graphics g;
        try{
            g = this.getGraphics();
            if((g != null) && (dbImage != null))
                g.drawImage(dbImage,0,0,null);
            Toolkit.getDefaultToolkit().sync();
        }catch(Exception e){
            System.out.println("Graphics context error: "+e);
        }
    }

	
	public static void main(String[] args) {
		JFrame app = new JFrame("El Mago Roberto");
		GamePanel gp = new GamePanel();
		app.getContentPane().add(gp, BorderLayout.CENTER);
		app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		app.pack();
		app.setResizable(false);
		app.setVisible(true);
	}

}
