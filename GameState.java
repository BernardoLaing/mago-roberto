import java.awt.Graphics;

public interface GameState {
	public void inicio(GameContext gc);
	public void load(GameContext gc);
	public void turn1(GameContext gc);
	public void turn2(GameContext gc);
	public void end(GameContext gc);
	public void render(Graphics g);
	public void update();
	public void setContext(GameContext gc);
	public void restart();
}
