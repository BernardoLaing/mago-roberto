import javax.swing.ImageIcon;

public class ImageLoader {
	
	private ImageIcon a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, background, start, one, two, three, blank, hang, hang1, hang2, hang3, hang4, hang5, dead;
	public static ImageLoader instance;
	
	private ImageLoader(){
		a = new ImageIcon(getClass().getResource("img/A.png"));
		b = new ImageIcon(getClass().getResource("img/B.png"));
		c = new ImageIcon(getClass().getResource("img/C.png"));
		d = new ImageIcon(getClass().getResource("img/D.png"));
		e = new ImageIcon(getClass().getResource("img/E.png"));
		f = new ImageIcon(getClass().getResource("img/F.png"));
		g = new ImageIcon(getClass().getResource("img/G.png"));
		h = new ImageIcon(getClass().getResource("img/H.png"));
		i = new ImageIcon(getClass().getResource("img/I.png"));
		j = new ImageIcon(getClass().getResource("img/J.png"));
		k = new ImageIcon(getClass().getResource("img/K.png"));
		l = new ImageIcon(getClass().getResource("img/L.png"));
		m = new ImageIcon(getClass().getResource("img/M.png"));
		n = new ImageIcon(getClass().getResource("img/N.png"));
		o = new ImageIcon(getClass().getResource("img/O.png"));
		p = new ImageIcon(getClass().getResource("img/P.png"));
		q = new ImageIcon(getClass().getResource("img/Q.png"));
		r = new ImageIcon(getClass().getResource("img/R.png"));
		s = new ImageIcon(getClass().getResource("img/S.png"));
		t = new ImageIcon(getClass().getResource("img/T.png"));
		u = new ImageIcon(getClass().getResource("img/U.png"));
		v = new ImageIcon(getClass().getResource("img/V.png"));
		w = new ImageIcon(getClass().getResource("img/W.png"));
		x = new ImageIcon(getClass().getResource("img/X.png"));
		y = new ImageIcon(getClass().getResource("img/Y.png"));
		z = new ImageIcon(getClass().getResource("img/Z.png"));
		background = new ImageIcon(getClass().getResource("img/Fondo.png"));
		start = new ImageIcon(getClass().getResource("img/Start.png"));
		one = new ImageIcon(getClass().getResource("img/1.png"));
		two = new ImageIcon(getClass().getResource("img/2.png"));
		three = new ImageIcon(getClass().getResource("img/3.png"));
		blank = new ImageIcon(getClass().getResource("img/blank.png"));
		hang = new ImageIcon(getClass().getResource("img/0Mistakes.png"));
		hang1  = new ImageIcon(getClass().getResource("img/1Mistake.png"));
		hang2  = new ImageIcon(getClass().getResource("img/2Mistakes.png"));
		hang3  = new ImageIcon(getClass().getResource("img/3Mistakes.png"));
		hang4  = new ImageIcon(getClass().getResource("img/4Mistakes.png"));
		hang5  = new ImageIcon(getClass().getResource("img/5Mistakes.png"));
		dead  = new ImageIcon(getClass().getResource("img/dead.png"));
	}
	
	public static ImageLoader getInstance(){
		if(instance == null){
			instance = new ImageLoader();
		}
		return instance;
	}
	
	public ImageIcon getImage(String tag){
		if(tag.equalsIgnoreCase("a"))
			return a;
		if(tag.equalsIgnoreCase("b"))
			return b;
		if(tag.equalsIgnoreCase("c"))
			return c;
		if(tag.equalsIgnoreCase("d"))
			return d;
		if(tag.equalsIgnoreCase("e"))
			return e;
		if(tag.equalsIgnoreCase("f"))
			return f;
		if(tag.equalsIgnoreCase("g"))
			return g;
		if(tag.equalsIgnoreCase("h"))
			return h;
		if(tag.equalsIgnoreCase("i"))
			return i;
		if(tag.equalsIgnoreCase("j"))
			return j;
		if(tag.equalsIgnoreCase("k"))
			return k;
		if(tag.equalsIgnoreCase("l"))
			return l;
		if(tag.equalsIgnoreCase("m"))
			return m;
		if(tag.equalsIgnoreCase("n"))
			return n;
		if(tag.equalsIgnoreCase("o"))
			return o;
		if(tag.equalsIgnoreCase("p"))
			return p;
		if(tag.equalsIgnoreCase("q"))
			return q;
		if(tag.equalsIgnoreCase("r"))
			return r;
		if(tag.equalsIgnoreCase("s"))
			return s;
		if(tag.equalsIgnoreCase("t"))
			return t;
		if(tag.equalsIgnoreCase("u"))
			return u;
		if(tag.equalsIgnoreCase("v"))
			return v;
		if(tag.equalsIgnoreCase("w"))
			return w;
		if(tag.equalsIgnoreCase("x"))
			return x;
		if(tag.equalsIgnoreCase("y"))
			return y;
		if(tag.equalsIgnoreCase("z"))
			return z;
		if(tag.equalsIgnoreCase("bg"))
			return background;
		if(tag.equalsIgnoreCase("start"))
			return start;
		if(tag.equalsIgnoreCase("1"))
			return one;
		if(tag.equalsIgnoreCase("2"))
			return two;
		if(tag.equalsIgnoreCase("3"))
			return three;
		if(tag.equalsIgnoreCase("blank"))
			return blank;
		if(tag.equalsIgnoreCase("0m"))
			return hang;
		if(tag.equalsIgnoreCase("1m"))
			return hang1;
		if(tag.equalsIgnoreCase("2m"))
			return hang2;
		if(tag.equalsIgnoreCase("3m"))
			return hang3;
		if(tag.equalsIgnoreCase("4m"))
			return hang4;
		if(tag.equalsIgnoreCase("5m"))
			return hang5;
		if(tag.equalsIgnoreCase("6m"))
			return dead;
		return null;
		
	}
	
}
