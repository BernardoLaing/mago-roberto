import java.awt.Graphics;

import javax.swing.ImageIcon;

public class Inicio implements GameState{
	private GameContext gc;
	private ImageIcon bg, startButton;
	
	public Inicio(){
		bg = ImageLoader.getInstance().getImage("bg");
		startButton = ImageLoader.getInstance().getImage("start");
		
	}

	@Override
	public void inicio(GameContext gc) {
		
	}

	@Override
	public void load(GameContext gc) {
		gc.setCurrent(gc.getLoad());
		
	}

	@Override
	public void turn1(GameContext gc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void turn2(GameContext gc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void end(GameContext gc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(Graphics g) {
		g.drawImage(bg.getImage(), 0, 0, 720, 405, null);
		g.drawImage(startButton.getImage(), 210, 240, 300, 100, null);
		
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setContext(GameContext gc) {
		this.gc = gc;
	}

	@Override
	public void restart() {
		// TODO Auto-generated method stub
		
	}
	
	
	

}
