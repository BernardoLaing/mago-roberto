import java.awt.Graphics;

import javax.swing.ImageIcon;



public class Letter {
	private char c;
	private int x, y;
	private ImageIcon letterImg,blank;
	private boolean guessed;
	
	public Letter(char c, int x, int y) {
		this.c = c;
		this.x = x;
		this.y = y;
		letterImg = ImageLoader.getInstance().getImage(Character.toString(this.c));
		blank = ImageLoader.getInstance().getImage("blank");
		guessed = false;
	}
	
	public void render(Graphics g) {
		if(guessed) {
			g.drawImage(letterImg.getImage(), x, y, 60, 60, null);
		}else {
			g.drawImage(blank.getImage(), x, y, 60, 60, null);
		}
	}
	
	public void setGuessed() {guessed = true;}
	public boolean guessed() {return guessed;}
	
	public char getC() {
		return c;
	}
	
}
