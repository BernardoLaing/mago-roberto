import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class Load implements GameState{
	
	private GameContext gc;
	private ImageIcon num, bg;
	private int count;
	
	public Load() {
		count = 0;
		num = ImageLoader.getInstance().getImage("3");
		bg = ImageLoader.getInstance().getImage("bg");

	}

	@Override
	public void inicio(GameContext gc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void load(GameContext gc) {
		
	}

	@Override
	public void turn1(GameContext gc) {
		gc.setCurrent(gc.getTurn1());
	}

	@Override
	public void turn2(GameContext gc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void end(GameContext gc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(Graphics g) {
		count += 1;
		g.drawImage(bg.getImage(), 0, 0, 720, 405, null);
		if(count > 200) {
			num = ImageLoader.getInstance().getImage("2");
		}
		if(count>400) {
			num = ImageLoader.getInstance().getImage("1");
		}
		g.drawImage(num.getImage(), 310, 200, 100, 100, null);
		if(count > 600) {
			gc.setCurrent(gc.getTurn1());
		}
		
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setContext(GameContext gc) {
		this.gc = gc;
		
	}

	@Override
	public void restart() {
		// TODO Auto-generated method stub
		
	}

}
