import java.awt.Graphics;

import javax.swing.ImageIcon;

/*
 * Player.java
 * 
 * Copyright 2016 Edgar Daniel Fernández Rodríguez <edgar.fernandez@me.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


public class Player {
	
	private int x, y, lives;
	private ImageIcon img;
	private boolean lost, won;
	
	
	public Player (int x, int y){
		this.x = x;
		this.y = y;
		img = ImageLoader.getInstance().getImage("0m");
		lives = 6;
	}
	
	public void render(Graphics g) {
		if(!lost && !won) {
			if(lives == 5) {
				img = ImageLoader.getInstance().getImage("1m");
			}else if(lives == 4) {
				img = ImageLoader.getInstance().getImage("2m");
			}else if(lives == 3) {
				img = ImageLoader.getInstance().getImage("3m");
			}else if(lives == 2) {
				img = ImageLoader.getInstance().getImage("4m");
			}else if(lives == 1) {
				img = ImageLoader.getInstance().getImage("5m");
			}else if(lives == 0) {
				img = ImageLoader.getInstance().getImage("6m");
			}
			g.drawImage(img.getImage(), x, y, 150, 150, null);
		}
	}
	
	public void wrong() {
		lives -= 1;
		if(lives<=0) {
			lose();
		}
	}
	
	public void lose() {
		lives = 0;
		lost = true;
	}
	
	public void win() {
		won = true;
	}
	
/*	public void wait(){
		currentState.wait();
	}*/
}

