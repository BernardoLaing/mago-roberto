import java.awt.Color;
import java.awt.Graphics;

import javax.swing.ImageIcon;

public class Turn1 implements GameState{
	private ImageIcon bg;
	private GameContext gc;

	public Turn1() {
		bg  = ImageLoader.getInstance().getImage("bg");
		
	}
	
	
	@Override
	public void inicio(GameContext gc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void load(GameContext gc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void turn1(GameContext gc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void turn2(GameContext gc) {
		gc.setCurrent(gc.getTurn2());
	}

	@Override
	public void end(GameContext gc) {
		gc.setCurrent(gc.getEnd());
	}

	@Override
	public void render(Graphics g) {
		Board.getInstance().render(g);
		g.setColor(Color.BLACK);
		g.drawRect(100, 100, 200, 200);
		
	}

	@Override
	public void update() {
		if(Board.getInstance().update()) {
			end(gc);
		}
		
	}

	@Override
	public void setContext(GameContext gc) {
		this.gc = gc;
		
	}

	@Override
	public void restart() {
		// TODO Auto-generated method stub
		
	}

}
