import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;

public class Word {
	private ArrayList<Letter> word;
	private Letter letter;
	private String[] wordBank;
	private String activeWord;
	
	public Word() {
		word = new ArrayList<Letter>();
		wordBank = new String[] {"roberto", "school", "happy",  "sun", "iphone", "edgar"};
		Random r = new Random();
//		activeWord = wordBank[r.nextInt(wordBank.length)];
		activeWord = "HOLA";
		generateWord();
	}
	
	public void generateWord() {
		int x = 360 - (60*(activeWord.length()/2));
		
		for(int i = 0; i < activeWord.length();  i++) {
			word.add(new Letter(activeWord.charAt(i), x, 310));
			x+=60;
		}
	}
	
	public void render(Graphics g) {
		for(int i = 0; i < word.size(); i++) {
			letter = word.get(i);
			letter.render(g);
		}
	}
	
	public boolean isGuessed() {
		for(int i = 0; i < word.size(); i++) {
			if(!word.get(i).guessed())
				return false;
		}
		return true;
	}
	
	public ArrayList<Letter> getWord(){
		return word;
	}
	
	
}
